# Build und deploy Microservices - Rosenheim Docker Meetup

This repository contains all files which where used in my demo. More details are available here: https://www.meetup.com/Rosenheim-Docker-Meetup/events/257190912/

Slides are available here: https://www.slideshare.net/nmeisenzahl/docker-meetup-rosenheim-package-deploy-microservices

## TL;DR

### app

A sample web-app based on Go

### helloworld-chart

A sample Helm chart. Deploys the demo app image.

### helloworld-cnab

A sample CNAB bundle. Deploys the demo app via a Helm chart.

### .gitlab-ci.yml

A sample build pipeline which uses a Kubernetes runner to build the demo app image.

### docker-compose-yaml

A sample Docker Compose definition. Deploys the demo app image.

### Dockerfile

A sample Dockerfile based on the demo app.

### docker_build.sh

A sample script to build the demo app image with Docker cli. 

### kaniko_docker.sh

A sample script to build the demo app image with Kaniko and Docker cli.

### kaniko_pod.sh

A sample script to build the demo app image with Kaniko in a Kubernetes environment.