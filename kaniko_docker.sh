#!/bin/bash
#Example - Docker build with Kaniko (no push)

docker run --rm \
  -v $(pwd):/workspace \
  gcr.io/kaniko-project/executor:latest \
  --dockerfile=./Dockerfile --context=/workspace --no-push
